<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>I-FEST 2019</title>
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/icon" href="assets/images/favicons.ico">
    <!-- Font Awesome -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Slick slider -->
    <link href="assets/css/slick.css" rel="stylesheet">
    <!-- Theme color -->
    <link id="switcher" href="assets/css/theme-color/default-theme.css" rel="stylesheet">

    <!-- Main Style -->
    <link href="style.css" rel="stylesheet">

    <!-- Fonts -->

    <!-- Open Sans for body font -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700,800" rel="stylesheet">
	<!-- Montserrat for title -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">



    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

  	<!-- Start Header -->
	<header id="mu-hero" class="" role="banner">
		<!-- Start menu  -->
		<nav class="navbar navbar-fixed-top navbar-default mu-navbar">
		  	<div class="container">
			    <!-- Brand and toggle get grouped for better mobile display -->
			    <div class="navbar-header">
			      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			      </button>

			      <!-- Logo -->
			      <a class="navbar-brand" href="index.php" >
  			      <img class="" src="assets/images/LUP4.0.png"
  			      style="width: 80px; height: 55px;">
  			      </a>

			    </div>

			    <!-- Collect the nav links, forms, and other content for toggling -->
			    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			      	<ul class="nav navbar-nav mu-menu navbar-right">
			      		<li><a href="#mu-hero">Home</a></li>
				        <li><a href="#mu-about">About Event</a></li>
				        <li><a href="#mu-schedule">Schedule</a></li>


			          <li><a href="#mu-guest">Konser</a></li>
<!-- 			            <li><a href="#mu-register">Video Teaser Acara</a></li>
 -->			      <li><a href="#mu-speakers">Seminar</a></li>
                <li><a href="#mu-lomba">Lomba</a></li>

			            <!-- <li><a href="#mu-sponsors">Sponsors</a></li>
			            <li><a href="#mu-contact">Contact us</a></li>
			      	 -->

			      	</ul>
			    </div><!-- /.navbar-collapse -->
		  	</div><!-- /.container-fluid -->
		</nav>
		<!-- End menu -->

		<div class="mu-hero-overlay">
			<div class="container">
				<div class="mu-hero-area">

					<!-- Start hero featured area -->
					<div class="mu-hero-featured-area">
						<!-- Start center Logo -->
						<div class="mu-logo-area">
							<!-- text based logo -->
							<a class="mu-logo" href="">I-FEST</a>
							<!-- image based logo -->
							<!-- <a class="mu-logo" href="#"><img src="assets/images/LUP4.0.png" alt="logo img"></a> -->
						</div>
						<!-- End center Logo -->

						<div class="mu-hero-featured-content">

							<h1>SELAMAT DATANG DI IT FESTIVAL</h1>
              <h2> Event Information Technology Sistem Informasi UINSA</h2>
							<p class="mu-event-date-line"> 25 April, 2019. UIN Sunan Ampel, SURABAYA</p>

							<div class="mu-event-counter-area">
								<div id="mu-event-counter">

								</div>
							</div>

						</div>
					</div>
					<!-- End hero featured area -->

				</div>
			</div>
		</div>
	</header>
	<!-- End Header -->

	<!-- Start main content -->
	<main role="main">
		<!-- Start About -->
		<section id="mu-about">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="mu-about-area">
							<!-- Start Feature Content -->
							<div class="row">
								<div class="col-md-6">
									<div class="mu-about-left">
										<img class="" src="assets/images/lup4.0.png" alt="Men Speaker">
									</div>
								</div>
								<div class="col-md-6">
                  <div class="mu-about-right">
                    <h2>Tentang I-FEST</h2>

                    <p>IT Festival atau yang biasa disebut IFest adalah event Information Technology tahunan yang diselenggarakan oleh mahasiswa program studi Sistem Informasi Uin Sunan Ampel Surabaya. IFest bertujuan mengenalkan berbagai macam inovasi serta kreativitas teknologi bagi anak muda. Tidak hanya itu, IFest juga menyediakan perlombaan sebagai ajang pengembangan bakat serta kreatifitas anak-anak muda dalam bidang IT. </p>

                    <p>IFest telah diselenggaran sebanyak 3 kali, yaitu pada tahun 2015, 2016, serta 2018. Dikemas semenarik mungkin dengan menghadirkan Gues Star yang dapat memotivasi anak muda serta sharing ilmu pengetahuan tentang IT. IFest  mempunyai isian acara, yaitu talk-show, seminar, lomba, serta isian acara pendamping lainnya. </p>

                    <p>Dari tahun ke tahun, IFest selalu hadir dengan konsep serta ide-ide baru yang fresh. IFest akan hadir kembali pada tahun 2019 mendatang pada bulan April. Tidak lupa ide yang fresh selalu hadir.</p>
                  </div>
								</div>
							</div>
							<!-- End Feature Content -->

						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End About -->

		<!-- Start Schedule  -->
		<section id="mu-schedule">
			<div class="container">
				<div class="row">
					<div class="colo-md-12">
						<div class="mu-schedule-area">

							<div class="mu-title-area">
								<h2 class="mu-title">Schedule Acara</h2>
                <p>acara diadakan 2 hari, pada sabtu 20 april 2019 dan kamis 25 april 2019.
                pada hari pertama akan ada seleksi Lomba cerdas cermat yang bertempat di Gedung Fakultas Sains dan Teknologi, sedangkan untuk business plan bertempat di Ruang Sidang Fakultas Sains dan Teknologi pada yaitu pada tanggal 20 April 2019.
                Seminar dan festival diadakan di gedung Sport Center and Multipurpose , Universitas Islam Negeri Sunan Ampel Surabaya pada tanggal 25 April 2019</p>
							</div>

							<div class="mu-schedule-content-area">
								<!-- Nav tabs -->
								<ul class="nav nav-tabs mu-schedule-menu" role="tablist" style="color: #fff;">
								    <li role="presentation" class="active"><a href="#first-day" aria-controls="first-day" role="tab" data-toggle="tab"> Day 1/ 20 April</a></li>
								    <li role="presentation"><a href="#second-day" aria-controls="second-day" role="tab" data-toggle="tab">2 Day / 25 April</a></li>
								</ul>

								<!-- Tab panes -->
								<div class="tab-content mu-schedule-content">
								    <div role="tabpanel" class="tab-pane fade mu-event-timeline in active" id="first-day">
								    	<ul>
								    		<li>
								    			<div class="mu-single-event">
								    				<p class="mu-event-time">8.00 AM</p>
								    				<h3>Pembukaan di sac lt 3</h3>
								    			</div>
								    		</li>
								    		<li>
								    			<div class="mu-single-event">
								    				<p class="mu-event-time">8.15 AM</p>
								    				<h3>Pengarahan lomba</h3>

								    			</div>
								    		</li>
								    		<li>
								    			<div class="mu-single-event">
                            <p class="mu-event-time">8.30 AM</p>
								    				<h3>masuk ke ruangan masing masing</h3>
								    			</div>
								    		</li>
								    		<li>
								    			<div class="mu-single-event">
								    				<p class="mu-event-time">9.00 AM</p>
								    				<h3>lomba dimulai</h3>
								    			</div>
								    		</li>
								    		<li>
								    			<div class="mu-single-event">
								    				<p class="mu-event-time">11.00 PM</p>
								    				<h3>lomba selesai</h3>
								    			</div>
								    		</li>
								    	</ul>
								    </div>
								    <div role="tabpanel" class="tab-pane fade mu-event-timeline" id="second-day">
								    	<ul>
								    		<li>
								    			<div class="mu-single-event">
								    				<p class="mu-event-time">9.00 AM</p>
								    				<h3>opening ceremony</h3>
								    			</div>
								    		</li>
								    		<li>
								    			<div class="mu-single-event">
								    				<img src="assets/images/speaker.jpg" alt="event speaker">
								    				<p class="mu-event-time">9.35 AM</p>
								    				<h3>seminar mitracomm indodax</h3>
								    				<span>By Oscar Darmawan</span>
								    			</div>
								    		</li>
								    		<li>
								    			<div class="mu-single-event">
								    				<p class="mu-event-time">11.40 AM</p>
								    				<h3>Penutupan seminar</h3>
								    			</div>
								    		</li>
								    		<li>
								    			<div class="mu-single-event">
								    				<p class="mu-event-time">11.45 - 12.30 AM </p>
								    				<h3>Break</h3>
								    				</div>
								    		</li>
								    		<li>
								    			<div class="mu-single-event">
								    				<p class="mu-event-time">1.00 PM</p>
								    				<h3>Opening Lomba</h3>
								    			</div>
								    		</li>
                        <li>
								    			<div class="mu-single-event">
								    				<p class="mu-event-time">1.00 - 3.00 PM</p>
								    				<h3>lomba 1</h3>
                            <span>cerdas cermat</span>
								    			</div>
								    		</li>
                        <li>
								    			<div class="mu-single-event">
								    				<p class="mu-event-time">3.00-4.45 PM</p>
								    				<h3>lomba 2</h3>
                            <span>Bussines plan</span>
								    			</div>
								    		</li>
								    	</ul>
								    </div>


								</div>

							</div>

						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End Schedule -->

		<!-- Start guest -->

		<section id="mu-guest">
			<div class="container">
					<div class="mu-guest-title">

            <h1>Guest Star</h1>
            </div>				<!-- Start Feature Content -->
              <img class="mu-guest-content" src="assets/images/guest.png">

							<!-- End Feature Content -->


			</div>
		</section>
		<!-- End Guest -->

		<!-- Start Video -->
		<section id="mu-video">
			<div class="mu-video-overlay">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="mu-video-area">
								<h2>tonton teaser</h2>
								<a class="mu-video-play-btn" href="#"><i class="fa fa-play" aria-hidden="true"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- Start Video content -->
			<div class="mu-video-content">
				<div class="mu-video-iframe-area">
					<a class="mu-video-close-btn" href="#"><i class="fa fa-times" aria-hidden="true"></i></a>
          <video width="75%" height="auto" src="assets/images/teaser.mp4" controls allowfullscreen></video>
        	</div>
			</div>
			<!-- End Video content -->

		</section>
		<!-- End Video -->

		<!-- Start Speakers -->
		<section id="mu-speakers">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="mu-speakers-area">

							<div class="mu-title-area">
								<h2 class="mu-title" style="color: black;">Pembicara Seminar</h2>
								<p>Teknologi digital dan komunikasi tumbuh berkembang dengan akselerasi tinggi. Dengan Masuknya revolusi industri keempat(industry 4.0), menekankan kepada integrasi antar alat menggunakan internet dan pemanfaatan big data. Kita juga perlu memperhatikan karakteristik dan bentuk-bentuk perkembangan teknologi saat ini, agar dapat memanfaatkannya secara maksimal dan menyiapkan langkah-langkah antisipasi yang tepat.hal tersebut sangatlah penting sehingga kami membuat seminar ini, Acara ini peruntukan untuk khalayak umum dengan mengusung tema :</p>
                <h4> “Optimalisasi teknologi untuk memenangkan persaingan di era Revolusi 4.0”</h4>
                <p>yang di isi oleh pemateri dari indodax </p>
							</div>

							<!-- Start Speakers Content -->

							<div class="mu-speakers-content" >
								<center>
								<div class="mu-speakers-slider" >


									<!-- Start single speaker -->
									<div class="mu-single-speakers" >
										<img src="assets/images/pembicara.jpg" alt="speaker img" >
										<div class="mu-single-speakers-info">
											<h3>Oscar Darmawan </h3>
											<p>Chief Executive Officer at Indodax Indonesia</p>

											<ul class="mu-single-speakers-social">
												<li><a href="#"><i class="fa fa-facebook"></i></a></li>
												<li><a href="#"><i class="fa fa-twitter"></i></a></li>
												<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
											</ul>
										</div>
									</div></center>
									<!-- End single speaker -->
								</div>
							</div>
							<!-- End Speakers Content -->

						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End Speakers -->

				<!-- Start lomba -->
				<section id="mu-schedule">
			<div class="container">
				<div class="row">
					<div class="colo-md-12">
						<div class="mu-schedule-area">

							<div class="mu-title-area">
								<h2 class="mu-title">Schedule Acara</h2>
                <p>acara diadakan 2 hari, pada sabtu 20 april 2019 dan kamis 25 april 2019.
                pada hari pertama akan ada seleksi Lomba cerdas cermat yang bertempat di Gedung Fakultas Sains dan Teknologi, sedangkan untuk business plan bertempat di Ruang Sidang Fakultas Sains dan Teknologi pada yaitu pada tanggal 20 April 2019.
                Seminar dan festival diadakan di gedung Sport Center and Multipurpose , Universitas Islam Negeri Sunan Ampel Surabaya pada tanggal 25 April 2019</p>
							</div>

							<div class="mu-schedule-content-area">
								</div>

								--<div class="card-block accordion-block">
											<div id="accordion" role="tablist" aria-multiselectable="true" >
													<div class="accordion-panel">
															<div class="accordion-heading" role="tab" id="headingOne">
																	<h3 class="card-title accordion-title">
																			<a class="accordion-msg waves-effect waves-dark scale_active" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" >
																			Lomba Bisnis Plan
																	</a>
															</h3>
													</div>
													<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
															<div class="accordion-content accordion-desc">
															<iframe src="https://docs.google.com/forms/d/e/1FAIpQLSdcOhXmrZF_kajUJToFxewZxJ2MD36Qajlx7w_BT0q2Ul5aBQ/viewform?usp=sf_link" height="200" width="300"></iframe>

															</div>
													</div>
											</div>
											<div class="accordion-panel">
													<div class="accordion-heading" role="tab" id="headingTwo">
															<h3 class="card-title accordion-title">
																	<a class="accordion-msg waves-effect waves-dark scale_active" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
																	Lomba cerdas cermat
															</a>
													</h3>
											</div>
											<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
													<div class="accordion-content accordion-desc">
													<iframe src="https://docs.google.com/forms/d/e/1FAIpQLSfsp1bcDWOhzqZ5OOnKAjvcZi4qYCmeGjBHMLBfaYH7Gvsj2A/viewform?usp=sf_link" height="200" width="300"></iframe>

											
													</div>
											</div>
									</div>
									
							</div>
					</div>
				</div>
				--


							</div>

						</div>
					</div>
				</div>
			</div>
		</section>

    		<!-- End Schedule -->


		<!-- Start Venue -->
		<section id="mu-venue">
			<div class="mu-venue-area">
				<div class="row">

					<div class="col-md-6">
						<div class="mu-venue-map">
              	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3957.280738012502!2d112.73206241416224!3d-7.322329994715514!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd7fb6c094d1b87%3A0xbc3def4f4bd2fa7!2sUIN+Sunan+Ampel!5e0!3m2!1sen!2sid!4v1547170567444" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
              	</div>
					</div>
          	<div class="col-md-6">
              <div class="mu-venue-address">
                <h2>Lokasi <i class="fa fa-chevron-right" aria-hidden="true"></i></h2>
                <h3>Sport Center and Multipurpose UIN Sunan Ampel</h3>
                <h4>Jl. Ahmad Yani No.117, Jemur Wonosari, Wonocolo, Kota SBY, Jawa Timur 60237</h4>
                <p>Acara ini diselenggarakan di dalam kampus uin sunan ampel surabaya, tepatnya di gedung sport center!</p>
              </div>
            </div>

				</div>
			</div>
		</section>
		<!-- End Venue -->


		<!-- Start Register  -->
		<!-- <section id="mu-register">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="mu-register-area">

							<div class="mu-title-area">
								<h2 class="mu-title">Register Form</h2>
						</div>

							<div class="mu-register-content">
								<form class="mu-register-form">

									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<input type="text" class="form-control" placeholder="Your Full Name" id="name" name="name" required="">
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<input type="email" class="form-control" placeholder="Enter Your Email" id="email" name="email" required="">
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<input type="text" class="form-control" placeholder="Your Phone Number" id="telephone" name="telephone" required="">
											</div>
										</div>
										<div class="col-md-6">

										<div class="form-group">
											<select class="form-control" name="ticket" id="ticket">
												<option value="0">Basic ($12)</option>
												<option value="1">Standard ($22)</option>
												<option value="2">Premium ($45)</option>
											</select>
										</div>
										</div>
									</div>

									<button type="submit" class="mu-reg-submit-btn">SUBMIT</button>

					            </form>
							</div>

						</div>
					</div>
				</div>
			</div>
		</section> -->
		<!-- End Register -->

		<!-- Start FAQ -->
		<!-- <section id="mu-faq">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="mu-faq-area">

							<div class="mu-title-area">
								<h2 class="mu-title">FAQ</h2>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint assumenda ut molestias doloremque ipsam, fugit laborum totam, pariatur est cumque at, repudiandae officia ex dolores quas minus optio, iusto soluta?</p>
							</div>

							<div class="mu-faq-content">

								<div class="panel-group" id="accordion">

							        <div class="panel panel-default">
							          <div class="panel-heading">
							            <h4 class="panel-title">
							              <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true">
							                <span class="fa fa-angle-down"></span> Lorem ipsum dolor sit amet.
							              </a>
							            </h4>
							          </div>
							          <div id="collapseOne" class="panel-collapse collapse in">
							            <div class="panel-body">
							              Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
							            </div>
							          </div>
							        </div>

							        <div class="panel panel-default">
							          <div class="panel-heading">
							            <h4 class="panel-title">
							              <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
							                <span class="fa fa-angle-up"></span> Lorem ipsum dolor sit amet, consectetur adipisicing elit.
							              </a>
							            </h4>
							          </div>
							          <div id="collapseTwo" class="panel-collapse collapse">
							            <div class="panel-body">
							              Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
							            </div>
							          </div>
							        </div>

							        <div class="panel panel-default">
							          <div class="panel-heading">
							            <h4 class="panel-title">
							              <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
							                <span class="fa fa-angle-up"></span> Lorem ipsum dolor sit amet, consectetur.
							              </a>
							            </h4>
							          </div>
							          <div id="collapseThree" class="panel-collapse collapse">
							            <div class="panel-body">
							              Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
							            </div>
							          </div>
							        </div>

							        <div class="panel panel-default">
							          <div class="panel-heading">
							            <h4 class="panel-title">
							              <a data-togxgle="collapse" data-parent="#accordion" href="#collapseFour">
							                <span class="fa fa-angle-up"></span> Lorem ipsum dolor sit amet, consectetur adipisicing.
							              </a>
							            </h4>
							          </div>
							          <div id="collapseFour" class="panel-collapse collapse">
							            <div class="panel-body">
							              Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
							            </div>
							          </div>
							        </div>

							        <div class="panel panel-default">
							          <div class="panel-heading">
							            <h4 class="panel-title">
							              <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
							                <span class="fa fa-angle-up"></span> Lorem ipsum dolor sit amet, consectetur.
							              </a>
							            </h4>
							          </div>
							          <div id="collapseFive" class="panel-collapse collapse">
							            <div class="panel-body">
							              Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
							            </div>
							          </div>
							        </div>


							    </div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</section> -->
		<!-- End FAQ  -->

		<!-- Start Sponsors -->

		<!-- End Sponsors -->


		<!-- Start Contact -->
		<!-- End Contact -->

	</main>

	<!-- End main content -->


	<!-- Start footer -->
	<footer id="mu-footer" role="contentinfo">
			<div class="container">
				<div class="mu-footer-area">
					<div class="mu-footer-top">
						<div class="mu-social-media">
							<a href="https://www.instagram.com/ifest2019/"><i class="fa fa-instagram"></i></a>
						</div>
					</div>
					<div class="mu-footer-bottom">
						<p class="mu-copy-right">&copy; Copyright <a rel="nofollow" href="http://markups.io">markups.io</a>. All right reserved.</p>
					</div>
				</div>
			</div>

	</footer>
	<!-- End footer -->



    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- Bootstrap -->
    <script src="assets/js/bootstrap.min.js"></script>
	<!-- Slick slider -->
    <script type="text/javascript" src="assets/js/slick.min.js"></script>
    <!-- Event Counter -->
    <script type="text/javascript" src="assets/js/jquery.countdown.min.js"></script>
    <!-- Ajax contact form  -->
    <script type="text/javascript" src="assets/js/app.js"></script>



    <!-- Custom js -->
	<script type="text/javascript" src="assets/js/custom.js"></script>




  </body>
</html>
